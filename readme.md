##Hidden Markov Models for POS Tagging

###Introduction
* Hidden Markov Model (HMM) is a statistical Markov model in which the system being modeled is assumed to be a Markov process with unobserved (i.e. hidden) states.
* Refer https://en.wikipedia.org/wiki/Hidden_Markov_model for more information

###Data
* The training file ```taggedcorpora.txt``` is present in the ```IO Files``` folder. It is in word/TAG format, with words separated by spaces and each sentence on a new line.
* The test data ```classify.txt``` is also present in the ```IO Files``` folder. It contains complete sentences with words separated by spaces.

###Programs
* The entrypoint is the ```main.py``` file. This file calls the functions defined in ```Trainer.py``` and ```Classify.py``` and generates the output (which is tagged words in sentences) in the file ```modelop.txt```