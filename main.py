import Trainer
import Classifier

trainingSet = "IO Files/taggedcorpora.txt"
model_filename = "IO Files/modelhmm.txt"
classification_file = "IO Files/classify.txt"
model_file = "IO Files/modelhmm.txt"
output_hmm_file = "IO Files/modelop.txt"

if __name__ == "__main__":
    """Training"""
    list_of_lines = [list.rstrip('\n') for list in open(trainingSet, "r")]
    emission_count_numerator, emission_count_denominator, transition_count,\
        last_word_tag_count, start_state_probabilities = Trainer.calculate(list_of_lines)
    subtracted_matrix = Trainer.create_subtracted_matrix(emission_count_denominator, last_word_tag_count)
    Trainer.write_to_model(emission_count_numerator, emission_count_denominator, transition_count, start_state_probabilities,
                           subtracted_matrix, len(list_of_lines), model_filename)

    """Classification"""
    emission_probabilities, transition_probabilities, \
    start_state_probabilities, possible_tag_list, subtracted_matrix = Classifier.read_model(model_filename)
    word_final_tag = Classifier.tag_sentences(possible_tag_list, list_of_lines,
                                              classification_file, transition_probabilities,
                                              emission_probabilities, start_state_probabilities,
                                              subtracted_matrix)
    Classifier.write_op_to_file(word_final_tag, output_hmm_file)