import math


#Reading the model file and storing emission probabilities, transition probabilities start-state probabilities
#and possible_tag_list
def read_model(model_file):

    emission_probabilities = {}
    transition_probabilities = {}
    start_state_probabilities = {}
    subtracted_matrix = {}

    list_of_lines = [list.rstrip("\n") for list in open(model_file,"r")]

    counts = list_of_lines[0].split(" ")
    emission_counts = int(counts[0])
    transition_counts = int(counts[1])
    start_state_counts = int(counts[2])

    for i in list_of_lines[1:emission_counts+1]:
            ep = i.split(" ")
            emission_probabilities[ep[0]]=float(ep[1])

    # print(emission_probabilities)

    for j in list_of_lines[emission_counts+1:emission_counts+transition_counts+1]:
        tp = j.split(" ")
        transition_probabilities[tp[0]]=float(tp[1])

    # print(transition_probabilities)

    for k in list_of_lines[1+emission_counts+transition_counts:1+emission_counts+transition_counts+start_state_counts]:
        ssp = k.split(" ")
        start_state_probabilities[ssp[0]]=float(ssp[1])

    # print(start_state_probabilities)

    possible_tag_list = list_of_lines[emission_counts+transition_counts+start_state_counts+1].split(' ')
    possible_tag_list.remove('')

    # print(possible_tag_list)

    for j in list_of_lines[emission_counts+transition_counts+start_state_counts+2:len(list_of_lines)-2]:
        subm = j.split(' ')
        subtracted_matrix[subm[0]] = float(subm[1])
    # print(subtracted_matrix)

    return emission_probabilities, transition_probabilities, \
           start_state_probabilities, possible_tag_list, subtracted_matrix


def tag_sentences(possible_tag_list, list_of_lines, classification_file,
                  transition_probabilities, emission_probabilities, start_state_probabilities, subtracted_matrix):

    no_of_tags = float(len(possible_tag_list))
    no_of_lines = float(len(list_of_lines)-1)

    #Main logic starts here
    list_of_dictionaries = []
    dict = {}

    c_list_of_lines = [list.rstrip('\n') for list in open(classification_file,'r')]

    i=0

    def maxfn(prev_word_dict, current_key, current_value):
        max = [-5000000,'']
        for wt in prev_word_dict:
            transition_str = wt[-2:] + '-' + current_key[-2:]
            if transition_str in transition_probabilities:
                trans_prob = transition_probabilities[transition_str]
            else:
                if wt[-2] in subtracted_matrix:
                    trans_prob = 1/(subtracted_matrix[wt[-2:]] + no_of_tags)
                else:
                    trans_prob = 1 / (0 + no_of_tags)
            answer = math.log(trans_prob) + prev_word_dict[wt][0] + math.log(current_value)
            if(answer > max[0]):
                max[0] = answer
                max[1] = wt[-2:]
        return max


    for line in c_list_of_lines:
        list_of_words = line.split(" ")
        for word in range(len(list_of_words)):
            dict = {}
            for tag in possible_tag_list:
                ep_word = list_of_words[word] + "/" + tag
                if ep_word in emission_probabilities:
                    dict[ep_word] = emission_probabilities[ep_word]

            if len(dict.keys()) == 0:
                for tag in possible_tag_list:
                    ep_word = list_of_words[word] + "/" + tag
                    dict[ep_word] = 1
            list_of_dictionaries.append(dict)

        for i in range(len(list_of_dictionaries)):
            if i == 0:
                for key in list_of_dictionaries[i]:
                    if key[-2:] in start_state_probabilities:
                        changed_value = [math.log(list_of_dictionaries[i][key]) + math.log(start_state_probabilities[key[-2:]]), '']
                    else:
                        changed_value = [math.log(list_of_dictionaries[i][key]) + math.log(1/(no_of_lines + no_of_tags)), '']

                    list_of_dictionaries[i][key] = changed_value
            else:
                for key in list_of_dictionaries[i]:
                    changed_value = maxfn(list_of_dictionaries[i - 1], key, list_of_dictionaries[i][key])
                    list_of_dictionaries[i][key] = changed_value

        word_final_tag = [""]
        where_it_came_from = ""
        lastdict = list_of_dictionaries[len(list_of_dictionaries)-1]
        maxval = -5000000
        for i in lastdict:
            if lastdict[i][0]>maxval:
                where_it_came_from  = lastdict[i][1]
                final_tag = i
                word_final_tag[0] = final_tag

        backindex = len(list_of_dictionaries)-2
        while backindex >= 0:
            for word_tag in list_of_dictionaries[backindex]:
                if word_tag[-2:] == where_it_came_from:
                    word_final_tag = [word_tag] + word_final_tag
                    where_it_came_from = list_of_dictionaries[backindex][word_tag][1]
                    break
            backindex = backindex-1

        return word_final_tag


def write_op_to_file(word_final_tag, filename):
    modelop = open(filename, "w")
    for i in range(len(word_final_tag)):
        if i!=len(word_final_tag)-1:
            modelop.write(word_final_tag[i])
            modelop.write(' ')
        else:
            modelop.write(word_final_tag[i])

    modelop.write('\n')