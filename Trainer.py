def calculate(list_of_lines):
    emission_count_numerator = {}
    emission_count_denominator = {}
    transition_count = {}
    last_word_tag_count = {}
    start_state_probabilities = {}

    for line in list_of_lines:
        wwt = line.split(" ")
        for word in range(len(wwt)):

            if wwt[word] in emission_count_numerator:
                emission_count_numerator[wwt[word]] += 1
            else:
                emission_count_numerator[wwt[word]] = 1

            tag1 = wwt[word][-2:]

            if word == 0:
                if tag1 in start_state_probabilities:
                    start_state_probabilities[tag1] += 1
                else:
                    start_state_probabilities[tag1] = 1;

            if word != len(wwt) - 1:
                tag2 = wwt[word + 1][-2:]
                transition_label = tag1 + '-' + tag2

                if transition_label in transition_count:
                    valtr = transition_count[transition_label] + 1
                    transition_count[transition_label] = valtr
                else:
                    transition_count[transition_label] = 1

            elif (word == len(wwt) - 1):
                if tag1 in last_word_tag_count:
                    vallw = last_word_tag_count[tag1] + 1
                    last_word_tag_count[tag1] = vallw
                else:
                    last_word_tag_count[tag1] = 1

            if tag1 in emission_count_denominator:
                val = emission_count_denominator[tag1] + 1
                emission_count_denominator[tag1] = val
            else:
                emission_count_denominator[tag1] = 1

    return emission_count_numerator, emission_count_denominator, \
           transition_count, last_word_tag_count, start_state_probabilities


# create a subtracted matrix
def create_subtracted_matrix(emission_count_denominator, last_word_tag_count):
    subtracted_matrix = {}
    for key in emission_count_denominator:
        if key in last_word_tag_count:
            newterm = emission_count_denominator[key] - last_word_tag_count[key]
        else:
            newterm = emission_count_denominator[key]
        subtracted_matrix[key] = newterm
    return subtracted_matrix


def write_to_model(emission_count_numerator, emission_count_denominator, transition_count, start_state_probabilities,
                   subtracted_matrix, length_line_list, filename):
    number_of_tags = len(emission_count_denominator)
    model_file = open(filename, "w")
    model_file.write(str(len(emission_count_numerator)))
    model_file.write(" ")
    model_file.write(str(len(transition_count)))
    model_file.write(" ")
    model_file.write(str(len(start_state_probabilities)))
    model_file.write("\n")

    # emission probabilities
    for key in emission_count_numerator:
        associated_tag = key[-2:]
        tag_count = emission_count_denominator[associated_tag]
        model_file.write(key)
        model_file.write(" ")
        model_file.write(str(emission_count_numerator[key] / tag_count))
        model_file.write("\n")

    # model_file.write('---TRANSITION PROBABILITIES---')

    for key in transition_count:
        start_tag = key[:2]
        model_file.write(key)
        model_file.write(' ')
        divided_trans_prob = (transition_count[key] + 1) / ((subtracted_matrix[start_tag]) + number_of_tags)
        model_file.write(str(divided_trans_prob))
        model_file.write('\n')
        dr_sub = 0

    # model_file.write('---START STATE PROBABILITIES---')
    for state in start_state_probabilities:
        model_file.write(state)
        model_file.write(' ')
        model_file.write(str((start_state_probabilities[state] + 1) / length_line_list + number_of_tags))
        model_file.write('\n')

    # writing possible tag sequence to the model file
    possible_tags = emission_count_denominator.keys()

    for i in possible_tags:
        model_file.write(i)
        model_file.write(' ')

    # writing the subtracted matrix to the model
    model_file.write('\n')
    for key in subtracted_matrix:
        model_file.write(key)
        model_file.write(' ')
        model_file.write(str(subtracted_matrix[key]))
        model_file.write('\n')

    model_file.write(str(length_line_list))